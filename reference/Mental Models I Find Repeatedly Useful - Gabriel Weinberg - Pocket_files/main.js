(function (ElementProto) {
    if (typeof ElementProto.matches !== 'function') {
        ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
            var element = this;
            var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
            var index = 0;

            while (elements[index] && elements[index] !== element) {
                ++index;
            }

            return Boolean(elements[index]);
        };
    }

    if (typeof ElementProto.closest !== 'function') {
        ElementProto.closest = function closest(selector) {
            var element = this;

            while (element && element.nodeType === 1) {
                if (element.matches(selector)) {
                    return element;
                }

                element = element.parentNode;
            }

            return null;
        };
    }
})(window.Element.prototype);

(function(){



    /*=================================
    =            Utilities            =
    =================================*/

    var domReady = function(callback) {
        document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
    };

    var getSessionId = function() {
        return window.sess_guid;
    };

    var ajax  = function(url, callback, data, cache) {

        if(data && typeof(data) === 'object') {
            var y = '', e = encodeURIComponent;
            for (x in data) {
                y += '&' + e(x) + '=' + e(data[x]);
            }
            data = y.slice(1) + (! cache ? '&_t=' + new Date : '');
        }

        try {
            var x = new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
            x.open(data ? 'POST' : 'GET', url, 1);
            x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            x.onreadystatechange = function () {
                x.readyState > 3 && callback && callback(x.responseText, x);
            };
            x.send(data)
        } catch (e) {
            window.console && console.log(e);
        }
    };

    function getNodeIndex(node) {
        var index = 0;
        while ( (node = node.previousSibling) ) {
            if (node.nodeType != 3 || !/^\s*$/.test(node.data)) {
                index++;
            }
        }
        return index;
    }

    var encodeURL = function(url){
        url = (url + '').toString();
        return url.replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    }

    // create a one-time event
    function onetime(node, type, callback) {

        // create event
        node.addEventListener(type, function(e) {
            // remove event
            e.target.removeEventListener(e.type, arguments.callee);
            // call handler
            return callback(e);
        });

    }

    /*=====  End of Utilities  ======*/



    /*====================================
    =            Item Actions            =
    ====================================*/

    var saveURL = function(item, url){

        if(signedout) return window.location.href = "/signup?src=pkt_explore";
        if(item.classList.contains('item_content_saved')) return;


        setSaving(item);
        var data = {
            web_request_key: getSessionId(),
            actions: JSON.stringify([{
                action: 'add',
                url: url,
                cxt_view: cxt_view,
                cxt_page: cxt_page,
                cxt_extra_content: item.getAttribute('data-extra'),
                cxt_item_position: getNodeIndex(item.parentNode.parentNode.parentNode.parentNode.parentNode) + 1
            }])
        };

        ajax('/v3/send', function( responseText, response ){
            if(response.status === 200){
                setSaved(item);
            }
            else{
                window.console && console.log(response.status);
            }

        }, data);
    }

    var trackLinkClick = function(item, isRecommendation){
        var isSpoc = !!parseInt(item.getAttribute('data-spoc'), 10) ? 1 : 0
        var extra_int = isRecommendation || isSpoc
        var data = {
            web_request_key: getSessionId(),
            actions: JSON.stringify([{
                view: 'web',
                section: cxt_view,
                identifier: 'click_item',
                page: document.body.getAttribute('data-page'),
                cxt_item_id : item.getAttribute('data-id'),
                extra_content: item.getAttribute('data-extra'),
                cxt_item_position: parseInt(item.getAttribute('data-position'),10) + 1,
                extra_int_data: extra_int
            }])
        };

        ajax('/v3/pv', function( responseText, response ){
            if(response.status === 200){
                // console.log(data)
            }
            else{
                window.console && console.log(response.status);
            }

        }, data);
    }

    /*
	    Generic click tracker for upsells
	    eventData should include
	    {
		    section,
		    identifier,
		    extra_int_data

		    optional:
		    page (ie the pathname)
		}

    */
    var trackUpsellClick = function(eventData) {
	    var data = {
            web_request_key: getSessionId(),
            actions: JSON.stringify([{
                view: 'web',
                section: eventData.section,
                identifier: eventData.identifier,
                page: document.body.getAttribute('data-page'),
                extra_int_data : eventData.extra_int_data
            }])
        };

        ajax('/v3/pv', function( responseText, response ){
            if(response.status === 200){
                // console.log(data)
            }
            else{
                window.console && console.log(response.status);
            }

        }, data);
    }

    var sendItemAction = function(action, context){
        context.action = action;
        var data = {
            web_request_key: getSessionId(),
            actions: JSON.stringify([context])
        };

        ajax('/v3/send', function( responseText, response ){
            if(response.status === 200){
                // console.log(data)
            }
            else{
                window.console && console.log(response.status);
            }

        }, data);
    }

    var setSaving = function(item){
        item.classList.add('item_content_saving');
        item.querySelectorAll('.item_content_savetext')[0].innerHTML = 'Saving';
    }

    var setSaved = function(item){
        item.classList.add('item_content_saved');
        item.querySelectorAll('.item_content_savetext')[0].innerHTML = 'Saved';
    }

    /*=====  End of Item Actions  ======*/


    var forEach = function (array, callback, scope) {
      for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
      }
    };

    var checkImages = function() {

        // Check for the images we currently have
        var imageList = document.querySelectorAll('.item_image');

        forEach(imageList, function(index, value)
        {
            // skip if we already have a placeholder set
            if (value.classList.contains('image_active'))
                return;

            var testImage = document.createElement('img')
                // imageTimeout = setTimeout(function(){
                //     unsetImages(value);
                //     testImage.remove();
                // },10000)

            testImage.addEventListener('error', function(){
                unsetImages(value)
            })

            testImage.addEventListener('load', function(){
                // clearTimeout(imageTimeout)
                if(this.naturalHeight < 150 || this.naturalWidth < 200) { unsetImages(value) }
                else{ value.classList.add('image_active') }
            })

            testImage.setAttribute('src', value.getAttribute('data-thumburl'))
        })

    }

    var unsetImages = function(rec) {
        var parent      = rec.parentNode;
        var colorArray  = ['teal', 'coral', 'goldenrod'];
            rec.classList.add(colorArray[rec.getAttribute('data-resolved-id') % 3]); // we use mod so items get consistent fallback images
            rec.classList.add('image_active')

    }


    var scrollCheck = function(value, element){
        if(element){
            if (value > 300){
                element.classList.add('active');
            } else {
                element.classList.remove('active');
            }
        }
    }
    var startScrollCheck = function(element){
        var ticking = false,
            last_known_scroll_position = 0;
        window.addEventListener('scroll', function(e) {
            last_known_scroll_position = window.scrollY;
            if (!ticking) {
                window.requestAnimationFrame(function() {
                    scrollCheck(last_known_scroll_position, element);
                    ticking = false;
                });
            }
            ticking = true;
        });
    }

    var removeIntro = function(){
        document.getElementById('portal_intro').classList.remove('intro_active');
    }

    var addIntro = function(){
        document.getElementById('portal_intro').classList.add('intro_active');
    }


    var setSponsorTracking = function(item){
        var trending_count  = (window.trending_count) ? window.trending_count : 0
        var best_of_count   = (window.best_of_count) ? window.best_of_count : 0
        var context     = {
            cxt_view            : 'explore',
            cxt_impression_id   : item.getAttribute('data-impressionid'),
            cxt_feed_item       : item.getAttribute('data-feeditemid'),
            cxt_item_position   : parseInt(item.getAttribute('data-sort_id'),10) + 1,
            cxt_trending_count  : trending_count,
            cxt_best_of_count   : best_of_count
        }

        //Impressioned
        sendItemAction('sp_impression_loaded', context)

        //Viewable
        var oav = new OpenAdViewability();
        oav.checkViewability(item, function(check){
            if(check.viewabiltyStatus) sendItemAction('sp_impression_viewed', context)
        });

        //Clicked
        var spoc_links = item.querySelectorAll('.spoc_link');

        // Add click handlers
        Array.prototype.forEach.call(spoc_links, function(el, i){
            el.addEventListener('click', function(e){
                sendItemAction('sp_impression_clicked', context)
            });
        });

    }


    /*=============================================
    =            Initialize on Dom Ready         =
    =============================================*/
    domReady(function(){




        // Get all save buttons
        var saveElements = document.querySelectorAll('.item_content_save');

        // Add click handlers
        Array.prototype.forEach.call(saveElements, function(el, i){
            el.addEventListener('click', function(e){
                e.preventDefault();
                saveURL(el, el.getAttribute('data-saveurl'))
            });
        });


        var items = document.querySelectorAll('.item');

        // Add click handlers on items
        Array.prototype.forEach.call(items, function(item, i){

            if(!!parseInt(item.getAttribute('data-spoc'), 10)){
                setSponsorTracking(item)
            }

            var trackLinks = item.querySelectorAll('.link_track');
            var position   = getNodeIndex(item);
            Array.prototype.forEach.call(trackLinks, function(link, j){
                var currentLink = link;
                var source_id = currentLink.getAttribute('data-source-article-id')
                currentLink.setAttribute('data-position', position)
                currentLink.addEventListener('click', function(){
                    trackLinkClick(currentLink, source_id)
                })
            })
        });

		// Add click handlers on various upsells on article page
		// To support one, add data-click-identifier and data-extra-int-data
		if ($)
		{
			$('a[data-click-identifier]').click(function(){

				var theLink = $(this);

				trackUpsellClick({
					section: 'syndicated',
					identifier: theLink.attr('data-click-identifier'),
					extra_int_data: theLink.attr('data-extra-int-data'),
				});
			});
		}

        var overflowTrigger = document.getElementById('overflow-trigger'),
            overflow        = document.getElementById('overflow-nav')

        overflowTrigger.addEventListener('click', function(e){
            e.preventDefault();
            overflow.classList.toggle('active');
            document.body.classList.toggle('overflow-active');
        });

        var spocTrigger     = document.querySelector('.spoc-overflow-trigger'),
            spocOverflow    = document.querySelector('.spoc-overflow'),
            spocHideTrigger = document.querySelector('.spoc-overflow-hide-this')

        var showPopup = function(e){
            e.preventDefault();
            e.stopPropagation();
            spocOverflow.classList.add('popover-active');
            onetime(document, "click", hidePopup);
        }

        var hidePopup = function(e){
            spocOverflow.classList.remove('popover-active');
        }

        var removeSpoc = function(e){
            e.preventDefault();
            spocTrigger.removeEventListener('click', showPopup);
            var item = e.target.closest('.item')
            item.parentNode.removeChild(item);

        }

        if(spocTrigger){
            spocTrigger.addEventListener('click', showPopup);
            spocHideTrigger.addEventListener('click', removeSpoc)
        }

        //checkImages()
        startScrollCheck(document.getElementById('related_topics'))

        // Add stickyfill handling for fixed scrolling sidebars
        $('.sticky').Stickyfill();


    });

    /*=====  End of Initialize on Dom Ready   =====*/


}());
